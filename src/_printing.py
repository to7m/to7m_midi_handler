from datetime import datetime
from pathlib import Path
from multiprocessing import Process, Queue
from traceback import format_exc


def print_daemon(queue, log_path):
    with log_path.open('a') as log_file:
        for string in iter(queue.get, object()):
            print(string, flush=True)
            print(string, file=log_file, flush=True)


def item_to_str_gen(items):
    no_previous_item = object()
    previous_item = no_previous_item

    for item in items:
        if isinstance(item, Exception):
            yield '\n    '
            yield format_exc()[:-1].replace('\n', '\n    ')
        else:
            if isinstance(previous_item, Exception):
                yield '\n'
            else:
                yield ' '

            yield str(item)


def print_factory(log_path):
    queue = Queue()
    Process(target=print_daemon, args=(queue, Path(log_path))).start()

    def print_(*items):
        items = item_to_str_gen(items)
        string = f"{datetime.now()}:{''.join(items)}"
        queue.put(string)
    return print_
