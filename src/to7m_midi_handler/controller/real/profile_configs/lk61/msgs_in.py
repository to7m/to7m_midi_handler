from ...config_tools import Assignments, DEFAULT, static_args


assignments = Assignments()


assignments("in0", [176, 103]).sign(static_args(-1))
assignments("in1", [191, 102]).sign(static_args(-1))
assignments("in0", [176, 102]).sign(static_args(1))
assignments("in1", [191, 103]).sign(static_args(1))


@assignments("in0", [224]).pitch_bend
def callback_factory_for_pitch_bend(pitch_bend):
    def callback(byte_list):
        msb = byte_list[2]
        lsb = 0 if msb < 64 else round((msb - 64) * (127 / 63))
        pitch_bend(lsb, msb)
    return callback


assignments("in0").note_on(DEFAULT)
assignments("in0").note_off(DEFAULT)



"""





    bm.add(port_purpose, partial_byte_list + [127], sign_callback)
    bm.discard(port_purpose, partial_byte_list + [0])




assigner("in0", [176, 103]).sign(lambda sign_method: sign_method(-1))



def ret_1(byte_list):
    return 1


for port_purpose, partial_byte_list in (("in0", [176, 102]),
                                        ("in1", [191, 103]):
    @assigner(port_purpose, partial_byte_list).sign
    def sign_positive_callback(byte_list):
        return 1


def sign_negative_callback(vc, bl):
    vc.controls.sign(-1)


def slider_factory(slider_num):
    def slider_callback(vc, bl):
        vc.controls.slider(slider_num, bl[2])
    return slider_callback


def button_factory(button_num):
    def button_callback(vc, bl):
        vc.controls.button(button_num)
    return button_callback


def button_0_callback(vc, bl):
    vc.controls.button_0()


def knob_factory(knob_num):
    def knob_callback(vc, bl):
        vc.controls.knob(knob_num, bl[2])
    return knob_callback


def pad_factory(pad_type, pad_num):
    if pad_type == "scene":
        def pad_depress_callback(vc, bl):
            vc.controls.scene_pad(True)

        def pad_release_callback(vc, bl):
            vc.controls.scene_pad(False)
    elif pad_type == "patch":
        def pad_depress_callback(vc, bl):
            vc.controls.patch_pad(True)

        def pad_release_callback(vc, bl):
            vc.controls.patch_pad(False)

    return pad_depress_callback, pad_release_callback


def pitch_bend_callback(vc, bl):
    msb = bl[2]
    lsb = 0 if msb < 64 else round((msb - 64) * (127 / 63))
    vc.controls.pitch_bend(lsb, msb)


assigner = Assigner()

assigner("in0", [176, 103]).sign(lambda: 0)


for port_purpose, partial_byte_list, sign_callback in (
        ("in0", [176, 103], sign_negative_callback),
        ("in1", [191, 102], sign_negative_callback),
        ("in0", [176, 102], sign_positive_callback),
        ("in1", [191, 103], sign_positive_callback)):
    bm.add(port_purpose, partial_byte_list + [127], sign_callback)
    bm.discard(port_purpose, partial_byte_list + [0])

for slider_num in range(1, 9):
    slider_callback = slider_factory(slider_num)
    bm.add("in0", [176, 40 + slider_num], slider_callback)
    bm.add("in1", [191, 40 + slider_num], slider_callback)
slider_callback = slider_factory(9)
bm.add("in0", [176, 7], slider_callback)
bm.add("in1", [191, 7], slider_callback)

for button_num in range(1, 10):
    button_callback = button_factory(button_num)
    bm.add("in0", [176, 50 + button_num, 127], button_callback)
    bm.add("in1", [191, 50 + button_num, 127], button_callback)
    bm.discard("in0", [176, 50 + button_num, 0])
    bm.discard("in1", [191, 50 + button_num, 0])

bm.discard("in1", [159, 12])
bm.add("in1", [159, 13, 0], lambda vc, bl: vc.controls.knobs_usable(False))
bm.add("in1", [159, 13, 127], lambda vc, bl: vc.controls.knobs_usable(True))
bm.add("in1", [159, 14, 0], lambda vc, bl: vc.controls.sliders_usable(False))
bm.add("in1", [159, 14, 127], lambda vc, bl: vc.controls.sliders_usable(True))
bm.add("in1", [159, 15, 0], button_0_callback)
bm.add("in1", [159, 15, 127], button_0_callback)
bm.discard("in1", [159, 16])

for knob_num in range(1, 9):
    knob_callback = knob_factory(knob_num)
    bm.add("in0", [176, 20 + knob_num], knob_callback)
    bm.add("in1", [191, 20 + knob_num], knob_callback)

for in_control in True, False:
    port_purpose = "in1" if in_control else "in0"
    channel = 15 if in_control else 9
    for pad_type in "scene", "patch":
        pad_byte_base = ((35, 43), (111, 115))[in_control][pad_type == "patch"]
        for pad_num in range(1, 9):
            pad_byte = pad_byte_base + pad_num
            if in_control and pad_num >= 5:
                pad_byte -= 20

            pad_depress_callback, pad_release_callback = pad_factory(pad_type,
                                                                     pad_num)
            bm.add(port_purpose, [144 | channel, pad_byte],
                   pad_depress_callback)
            bm.add(port_purpose, [128 | channel, pad_byte, 0],
                   pad_release_callback)

for in_control in True, False:
    port_purpose = "in1" if in_control else "in0"
    byte_0_to_add = 159 if in_control else 176
    byte_0_to_discard = 143 if in_control else 176
    byte_1_to_write, byte_1_to_revert = ((104, 105), (104, 120))[in_control]

    bm.add(port_purpose, [byte_0_to_add, byte_1_to_write, 127],
           lambda vc, bl: vc.controls.saved_scene_manage("write"))
    bm.add(port_purpose, [byte_0_to_add, byte_1_to_revert, 127],
           lambda vc, bl: vc.controls.saved_scene_manage("revert"))
    bm.discard(port_purpose, [byte_0_to_discard, byte_1_to_write, 0])
    bm.discard(port_purpose, [byte_0_to_discard, byte_1_to_revert, 0])

bm.add("in1", [191, 112, 127], lambda vc, bl: vc.controls.patch_decrement())
bm.discard("in1", [191, 112, 0])
bm.add("in1", [191, 113, 127], lambda vc, bl: vc.controls.patch_increment())
bm.discard("in1", [191, 113, 0])
bm.add("in1", [191, 114, 127], lambda vc, bl: vc.controls.stop(True))
bm.add("in1", [191, 114, 0], lambda vc, bl: vc.controls.stop(False))
bm.add("in1", [191, 115, 127], lambda vc, bl: vc.controls.enter())
bm.discard("in1", [191, 115, 0])
bm.add("in1", [191, 116, 127], lambda vc, bl: vc.controls.saved_knobs_manage())
bm.discard("in1", [191, 116, 0])
bm.add("in1", [191, 117, 127], lambda vc, bl: vc.controls.toggle_record())
bm.discard("in1", [191, 117, 0])

bm.add("in0", [224], pitch_bend_callback)
bm.add("in0", [176, 1], lambda vc, bl: vc.controls.mod_wheel(bl[2]))

bm.add("in0", [128], lambda vc, bl: vc.controls.note_off(bl[1]))
bm.add("in0", [144], lambda vc, bl: vc.controls.note_on(bl[1], bl[2]))

bm.add("in0", [176, 67], lambda vc, bl: vc.controls.pedal_soft(bl[2]))
bm.add("in0", [176, 66], lambda vc, bl: vc.controls.pedal_sostenuto(bl[2]))
bm.add("in0", [176, 64], lambda vc, bl: vc.controls.pedal_sustain(bl[2]))
"""
