from ...config_tools import ConnectInfo


connect_info = ConnectInfo(
    ["Launchkey MK2 61"],
    [((0, 0), "in0"), ((0, 1), "in1")],
    [("out0", (0, 0)), ("out1", (0, 1))],
    0
)
