from ...config_tools import ConnectInfo


connect_info = ConnectInfo(
    ["KeyLab mkII 88"],
    [((0, 0), "in0"), ((0, 1), "in1")],
    [("out0", (0, 0)), ("out1", (0, 1))],
    1
)
