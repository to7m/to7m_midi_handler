from threading import Thread, Lock

from .....io_.staggered_queue import StaggeredQueue
from ...config_tools import ToDeviceBase
from .factory_msgs import factory_msgs


LAST = object()


class ToDevice(ToDeviceBase):
    def __init__(self, print_, sends):
        super().__init__(print_, sends)

        self._staggered_queue = StaggeredQueue(print_, 0.0005)
        self._staggered_queue.open()
        self._lock = Lock()
        self._connected = True
        Thread(target=self._unpack_and_send).start()

    def _unpack_and_send(self):
        for item in iter(self._staggered_queue.get, LAST):
            with self._lock:
                if self._connected:
                    for port_purpose, byte_list in item:
                        self._sends[port_purpose](byte_list)
        self._staggered_queue.close()

    def _low_power_mode_on(self):
        """
        prevents power-up light show
        """

        byte_list = [240, 0, 32, 107, 127, 66, 2, 0, 64, 81, 1, 247]
        self._staggered_queue.put("low power mode", [("out0", byte_list)])

    def _vegas_mode_off(self):
        """
        prevents idle light show
        """

        byte_list = [240, 0, 32, 107, 127, 66, 2, 0, 64, 80, 0, 247]
        self._staggered_queue.put("vegas mode", [("out0", byte_list)])

    def _send_user_memory_byte_list(self, key, byte_list):
        self._staggered_queue.put(key, [("out0", byte_list),
                                        ("out0", factory_msgs["save"])])

    def _sustain_pedal_mode_control(self):
        byte_list = [240, 0, 32, 107, 127, 66, 2, 3, 1, 80, 1, 247]
        self._send_user_memory_byte_list("sustain pedal mode", byte_list)

    def connect(self):
        for msg_num, byte_list in factory_msgs.items():
            self._staggered_queue.put((msg_num, "initial"),
                                      [("out0", byte_list)])
        self._low_power_mode_on()
        self._vegas_mode_off()
        self._sustain_pedal_mode_control()

    def disconnect(self):
        with self._lock:
            self._connected = False
