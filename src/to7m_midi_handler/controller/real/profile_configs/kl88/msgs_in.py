from ...config_tools import Assignments, DEFAULT


assignments = Assignments()


assignments.ignore("in1", [240, 0, 32, 107, 127, 66, 2, 0, 0, 21, 127, 247])

assignments("in0").pitch_bend(DEFAULT)

assignments("in0").note_on(DEFAULT)
assignments("in0").note_off(DEFAULT)
