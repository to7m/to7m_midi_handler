from ....exceptions import ConfigToolsError


class ConnectInfo:
    """
    Used by ....io_.connector.Connector.
    """

    def __init__(self, client_names,
                 controllers_to_port_purposes, port_purposes_to_controllers,
                 priority=0):
        """
        args:
            client_names:
                List of client names as shown in ‘aconnect -l’.

            controllers_to_port_purposes:
                List of tuples in format ((client_i, port_num), port_purpose),
                where client_i is the index of the client name in client_names,
                port_num is the port number for that client as listed in
                aconnect, and port_purpose is of the format ‘in{x}’ where x is
                in 0...3.

            port_purposes_to_controllers:
                List of tuples in format (port_purpose, (client_i, port_num)),
                where client_i is the index of the client name in client_names,
                port_num is the port number of that client as listed in
                aconnect, and port_purpose is of the format ‘out{x}’ where x is
                in 0...3.

            priority:
                Number; the higher it is, the higher the precedence the profile
                will be given when client_names in different profiles clash or
                when the maximum number (4) of profiles are loaded.
        """

        self.client_names = client_names

        self._check_clients_listed()

        self._client_indices_referenced = set()

        self.in_ = self._dict("in", controllers_to_port_purposes)
        self.out = self._dict("out", port_purposes_to_controllers)
        self.priority = priority

        self._check_clients_referenced()

    def _dict(self, port_purpose_name, connections):
        dict_ = {f"{port_purpose_name}{i}": None for i in range(4)}
        port_infos = set()
        for item in connections:
            if port_purpose_name == "in":
                port_info, port_purpose = item
            elif port_purpose_name == "out":
                port_purpose, port_info = item

            if dict_[port_purpose] is not None:
                raise ConfigToolsError(f"port purpose {port_purpose} reused")

            dict_[port_purpose] = port_info

            if port_info in port_infos:
                raise ConfigToolsError(
                    f"repeated port reference {port_info} in same direction")
            port_infos.add(port_info)

            client_i, _ = port_info
            self._client_indices_referenced.add(client_i)

        return dict_

    def _check_clients_listed(self):
        if not self.client_names:
            raise ConfigToolsError("no client names given")

    def _check_clients_referenced(self):
        declared_first = list(range(len(self.client_names)))
        referenced = sorted(self._client_indices_referenced)
        if declared_first != referenced:
            raise ConfigToolsError("not all client names given are used")
