DEFAULT = object()
IGNORE = object()


class Assignment:
    def __init__(self, port_purpose, partial_byte_list,
                 controls_attr_name, callback_factory_for_method):
        self.port_purpose = port_purpose
        self.partial_byte_list = partial_byte_list
        self.controls_attr_name = controls_attr_name
        self.callback_factory_for_method = callback_factory_for_method


class _DefaultsAssignerFor:
    def __init__(self, partial_byte_lists, callback_factories_for_methods,
                 partial_byte_list):
        self._partial_byte_lists = partial_byte_lists
        self._callback_factories_for_methods = callback_factories_for_methods
        self._partial_byte_list = partial_byte_list

    def __getattr__(self, controls_attr_name):
        def take_callback_factory(callback_factory_for_method):
            self._partial_byte_lists[controls_attr_name] \
                = self._partial_byte_list
            self._callback_factories_for_methods[controls_attr_name] \
                = callback_factory_for_method
            return callback_factory_for_method
        return take_callback_factory


class _Defaults:
    def __init__(self):
        self._partial_byte_lists = {}
        self._callback_factories_for_methods = {}

    def __call__(self, partial_byte_list):
        return _DefaultsAssignerFor(self._partial_byte_lists,
                                    self._callback_factories_for_methods,
                                    partial_byte_list)

    def get_assignment(self, port_purpose, controls_attr_name):
        return Assignment(
            port_purpose, self._partial_byte_lists[controls_attr_name],
            controls_attr_name,
            self._callback_factories_for_methods[controls_attr_name]
        )


defaults = _Defaults()


@defaults([224]).pitch_bend
def _callback_factory_for_pitch_bend(pitch_bend):
    def callback(byte_list):
        pitch_bend(byte_list[1], byte_list[2])
    return callback


@defaults([144]).note_on
def _callback_factory_for_note_on(note_on):
    def callback(byte_list):
        note_on(byte_list[1], byte_list[2])
    return callback


@defaults([128]).note_off
def _callback_factory_for_note_off(note_off):
    def callback(byte_list):
        note_off(byte_list[1])
    return callback


class _AssignerFor:
    def __init__(self, assignments, port_purpose, partial_byte_list):
        self._assignments = assignments
        self._port_purpose = port_purpose
        self._partial_byte_list = partial_byte_list

    def __getattr__(self, controls_attr_name):
        def adder(callback_factory_for_method):
            if callable(callback_factory_for_method):
                assignment = Assignment(
                    self._port_purpose, self._partial_byte_list,
                    controls_attr_name, callback_factory_for_method
                )
            elif callback_factory_for_method == DEFAULT:
                assignment = defaults.get_assignment(self._port_purpose,
                                                     controls_attr_name)
            else:
                raise TypeError(callback_factory_for_method)

            self._assignments.append(assignment)

            return callback_factory_for_method
        return adder


class Assignments:
    def __init__(self):
        self._assignments = []

    def __call__(self, port_purpose, partial_byte_list=None):
        return _AssignerFor(self._assignments, port_purpose, partial_byte_list)

    def __iter__(self):
        return iter(self._assignments)

    def ignore(self, port_purpose, partial_byte_list):
        assignment = Assignment(port_purpose, partial_byte_list, IGNORE, None)
        self._assignments.append(assignment)


def static_args(*xargs):
    if eval(repr(xargs)) != xargs:
        raise ValueError(f"xargs {xargs} are not repr-complete")

    callback_factory_for_method = eval(
        f"lambda method: lambda byte_list: method{repr(xargs)}"
    )

    return callback_factory_for_method
