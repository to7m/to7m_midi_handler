from pathlib import Path
from importlib import import_module

from to7m_midi_handler.velocity_curves import (render_velocity_maps,
                                               DummyVelocityCurvesForPatches)
from . import profile_configs
from ._callbacks import DummyCallbacksFactory, CallbacksFactory
from .config_tools.to_device import ToDeviceBase


class _DummyProfile:
    def __init__(self):
        self.callbacks_factory = DummyCallbacksFactory()
        self.ToDevice = ToDeviceBase
        self.velocity_curves_for_patches = DummyVelocityCurvesForPatches()


DUMMY_PROFILE = _DummyProfile()


class _Profile:
    def __init__(self, print_, pkg, patches_dict):
        self.connect_info = pkg.connect_info
        self.callbacks_factory = CallbacksFactory(print_, pkg.assignments)
        self.ToDevice = pkg.ToDevice
        self.velocity_curves_for_patches = render_velocity_maps(
                                               pkg.controller_map,
                                               patches_dict)


def get_profiles(print_, patches_dict):
    profile_configs_dir = Path(__file__).parent / "profile_configs"

    profiles_dict = {}
    for module_file_or_pkg_dir in profile_configs_dir.iterdir():
        profile_name = module_file_or_pkg_dir.name
        if profile_name.startswith('_'):
            continue

        if profile_name[-3:] == ".py":
            profile_name = profile_name[:-3]

        relative_path_for_pkg = f".{profile_name}"
        profile_pkg = import_module(relative_path_for_pkg,
                                    profile_configs.__name__)
        profiles_dict[profile_name] = _Profile(print_,
                                               profile_pkg, patches_dict)

    profiles_dict = dict(
        sorted(profiles_dict.items(),
               key=lambda item: -item[1].connect_info.priority)
    )

    return profiles_dict
