from .config_tools import IGNORE


PORT_PURPOSES = "in0", "in1", "in2", "in3"


def _report_factory(print_, port_purpose):
    def report(byte_list):
        msg = port_purpose, byte_list
        print_("unrecognised msg:", msg)
    return report


def _ignore(byte_list):
    pass


class DummyCallbacksFactory:
    def __call__(self, controls):
        callbacks = {port_purpose: _ignore for port_purpose in PORT_PURPOSES}
        return callbacks


class CallbacksFactory:
    def __init__(self, print_, assignments):
        self._assignments = assignments

        self._reports = {port_purpose: _report_factory(print_, port_purpose)
                         for port_purpose in PORT_PURPOSES}

    def __call__(self, controls):
        callbacks = {port_purpose: [self._reports[port_purpose]
                                    for _ in range(256)]
                     for port_purpose in PORT_PURPOSES}

        for assignment in self._assignments:
            callback = self._callback_factory(controls, assignment)

            list_ = callbacks[assignment.port_purpose]
            for byte in assignment.partial_byte_list[:-1]:
                if callable(list_[byte]):
                    list_[byte] = [self._reports[assignment.port_purpose]
                                   for _ in range(256)]

                list_ = list_[byte]
            list_[assignment.partial_byte_list[-1]] = callback

        return callbacks

    def _callback_factory(self, controls, assignment):
        if isinstance(assignment.controls_attr_name, str):
            method = getattr(controls, assignment.controls_attr_name)
            callback = assignment.callback_factory_for_method(method)
        elif assignment.controls_attr_name is IGNORE:
            callback = _ignore

        return callback
