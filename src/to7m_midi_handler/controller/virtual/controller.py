import numpy as np
from multiprocessing import Process

from ...exceptions import MsgHandlingError
from ...io_.ports import ControllerPorts
from ..real.profiles import DUMMY_PROFILE
from ._msgs_to_int7 import msg_method_factories


class _State:
    def __init__(self, print_, msg_methods, patches_dict):
        """
        Called once.
        """

        self._print = print_
        self._msg_methods = msg_methods
        self._patches_dict = patches_dict
        self._init(DUMMY_PROFILE)

    def _init(self, profile=DUMMY_PROFILE, sends=None):
        """
        Called for every connection change.
        """

        self._to_device = profile.ToDevice(self._print, sends)

        self.channel_offset = 0

        self._mode = "tap tempo"

        self._sign = 1
        self._current_digits = []

        self._patch = self._patches_dict[(1, 2, 3)]
        self.velocity_map \
            = profile.velocity_curves_for_patches[self._patch.tuple]

    def _reset_number(self):
        self.sign = 1
        self.current_digits = []

    def set_mode(self, mode):
        if mode == "tap tempo":
            ...
        elif mode == "knob-controlled parameter":
            self._reset_number()
        else:
            raise MsgHandlingError(f"invalid mode: {mode}")

        self.mode = mode

    def set_sign(self, sign):
        if sign in (1, -1):
            self.sign = sign
        else:
            raise MsgHandlingError(f"invalid sign: {sign}")

    def add_digit(self, digit):
        if isinstance(digit, int) and digit in range(10):
            self.current_digits.append(digit)
        else:
            raise MsgHandlingError(f"invalid digit: {digit}")

    def connect_to_vc(self, profile, sends):
        self._init(profile, sends)
        self._to_device.connect()

    def disconnect_from_vc(self):
        self._msg_methods.all_notes_off()
        self._to_device.disconnect()
        self._init()

    def connect_to_int7(self):
        ...


class _Controls:
    def __init__(self, msg_methods, state):
        self._msg_methods = msg_methods
        self._state = state

    def sign(self, sign_):
        self._state.set_mode("knob-controlled parameter")
        self._state.set_sign(sign_)

    def pitch_bend(self, lsb, msb):
        self._msg_methods.pitch_bend(self._state.channel_offset, lsb, msb)

    def note_on(self, note, velocity):
        velocity = self._state.velocity_map[note, velocity]
        self._msg_methods.note_on(self._state.channel_offset, note, velocity)

    def note_off(self, note):
        self._msg_methods.note_off(note)


class VirtualController(Process):
    def __init__(self, print_, vc_num, profiles_dict, patches_dict):
        super().__init__()

        self._print = print_
        self._profiles_dict = profiles_dict

        channel_base = vc_num * 4
        ports = ControllerPorts(vc_num)
        msg_methods = msg_method_factories(ports.int7, channel_base)

        self._state = _State(print_, msg_methods, patches_dict)
        self._controls = _Controls(msg_methods, self._state)

        self._receive = ports.receive
        self._sends = ports.sends
        self.inject = ports.inject

    def run(self):
        profile = DUMMY_PROFILE
        callbacks = profile.callbacks_factory(self._controls)
        while True:
            try:
                try:
                    for item in iter(self._receive, object()):
                        port_purpose, byte_list = item

                        callback = callbacks[port_purpose]
                        for byte in byte_list:
                            if callable(callback):
                                break

                            callback = callback[byte]
                        callback(byte_list)
                except ValueError:
                    (action, arg), = item

                    if action == "connect to int7":
                        self._state.connect_to_int7()
                    elif action == "connection change":
                        profile = DUMMY_PROFILE
                        self._state.disconnect_from_vc()

                        if arg:
                            profile_name = arg
                            profile = self._profiles_dict[profile_name]
                            self._state.connect_to_vc(profile, self._sends)

                        callbacks = profile.callbacks_factory(self._controls)
                    else:
                        raise ValueError(f"unrecognised action: {action}")
            except Exception as err:
                self._print(f"handling of message {item} caused error:", err)
