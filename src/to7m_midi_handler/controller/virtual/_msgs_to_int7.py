class _MsgMethodFactories:
    def __init__(self):
        self._factories = {}

    def __getattr__(self, attr_name):
        def add_to_factories(msg_method_factory):
            self._factories[attr_name] = msg_method_factory
            return msg_method_factory
        return add_to_factories

    def __call__(self, send_to_int7, channel_base):
        return _MsgMethods(self._factories, send_to_int7, channel_base)


class _MsgMethods:
    def __init__(self, factories, send_to_int7, channel_base):
        for attr_name, msg_method_factory in factories.items():
            msg_method = msg_method_factory(send_to_int7, channel_base)
            setattr(self, attr_name, msg_method)


def range_to_plus_4(num):
    return range(num, num + 4)


msg_method_factories = _MsgMethodFactories()


@msg_method_factories.pitch_bend
def _pitch_bend_factory(send_to_int7, channel_base):
    byte_0_base = 224 | channel_base

    def pitch_bend(channel_offset, lsb, msb):
        send_to_int7([byte_0_base | channel_offset, lsb, msb])
    return pitch_bend


@msg_method_factories.note_on
def _note_on_factory(send_to_int7, channel_base):
    byte_0_base = 144 | channel_base

    def note_on(channel_offset, note, velocity):
        send_to_int7([byte_0_base | channel_offset, note, velocity])
    return note_on


@msg_method_factories.note_off
def _note_off_factory(send_to_int7, channel_base):
    byte_0_range = range_to_plus_4(128 | channel_base)

    def note_off(note):
        for byte_0 in byte_0_range:
            send_to_int7([byte_0, note, 0])
    return note_off


@msg_method_factories.all_notes_off
def _all_notes_off_factory(send_to_int7, channel_base):
    byte_0_range = range_to_plus_4(176 | channel_base)

    def all_notes_off():
        for byte_0 in byte_0_range:
            send_to_int7([byte_0, 123, 0])
    return all_notes_off
