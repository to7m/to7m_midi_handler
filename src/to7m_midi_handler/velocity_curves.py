import numpy as np


notes = np.arange(128, dtype=np.single)
notes.reshape((128, 1))
velocities = np.arange(128)
controller_map_base = np.empty((128, 128), dtype=np.single)
controller_map_base[:] = velocities

for static_arr in notes, velocities, controller_map_base:
    static_arr.flags.writeable = False


def from_white_black(white_map, black_map):
    is_black = (notes + 1) * 5 % 12 >= 7
    return np.where(is_black, black_map, white_map)


def to_int(float_map, ones_to_ones=True):
    as_int = np.uint8(np.clip(float_map.copy() + 0.5, 1, 127))
    as_int[:, 0] = 0
    if ones_to_ones:
        as_int[:, 1] = 1
    return as_int


class DummyVelocityCurvesForPatches:
    zeros_int = np.zeros((128, 128), dtype=np.uint8)

    def __getitem__(self, index):
        return self.zeros_int


def render_velocity_maps(controller_map, patches_dict):
    velocity_curves_for_patches = {
        patch_tuple: patch.map_from_controller(controller_map)
        for patch_tuple, patch in patches_dict.items()
    }
    return velocity_curves_for_patches
