from pathlib import Path
from importlib import import_module

from to7m_midi_handler.exceptions import PatchConfigError
from . import patch_configs


class _Patch:
    def __init__(self, patch_tuple, pkg):
        self.tuple = patch_tuple
        self.map_from_controller = pkg.map_from_controller


def _parse_patch_digits(patch_digits, patches_dict):
    if patch_digits[0] == '_':
        return

    digits = []
    for char in patch_digits:
        if '1' <= char <= '8':
            int_ = int(char)
            if int_ in digits:
                raise PatchConfigError(
                          f"repeated patch digits in {patch_digits}")

            digits.append(int_)
        else:
            raise PatchConfigError(
                      f"invalid patch digit character in {patch_digits}")

    patch_tuple = tuple(digits)
    if patch_tuple in patches_dict:
        raise PatchConfigError(f"repeated patch tuple: {patch_tuple}")

    return patch_tuple


def get_patches():
    patch_configs_dir = Path(__file__).parent / "patch_configs"

    patches_dict = {}
    for module_file_or_pkg_dir in patch_configs_dir.iterdir():
        patch_digits = module_file_or_pkg_dir.name[:3]
        patch_tuple = _parse_patch_digits(patch_digits, patches_dict)
        if not patch_tuple:
            continue

        if module_file_or_pkg_dir.name[-3:] == ".py":
            patch_name = module_file_or_pkg_dir.name[4:-3]
        else:
            patch_name = module_file_or_pkg_dir.name[4:]

        relative_path_for_pkg = f".{patch_digits}_{patch_name}"
        patch_pkg = import_module(relative_path_for_pkg,
                                  patch_configs.__name__)
        patches_dict[patch_tuple] = _Patch(patch_tuple, patch_pkg)

    return patches_dict
