from multiprocessing import Queue

from .python_rtmidi_wrappers import (
    midi_in_set_queue,
    midi_out_return_send, midi_out_set_queue, midi_out_staggered_queue
)


class ControllerPorts:
    def __init__(self, controller_num):
        in_queue = Queue()
        for port_purpose in ("in0", "in1", "in2", "in3"):
            midi_in_set_queue(f"controller{controller_num}-{port_purpose}",
                              in_queue, port_purpose)
        self.receive = in_queue.get
        self.inject = in_queue.put

        self.sends = {
            port_purpose: midi_out_return_send(
                              f"controller{controller_num}-{port_purpose}")
            for port_purpose in ("out0", "out1", "out2", "out3")
        }

        self.int7 = midi_out_return_send(f"controller{controller_num}-int7")


class GlobalInt7Ports:
    def __init__(self, print_):
        no_delay_queue = Queue()
        midi_out_set_queue("global_int7-no_delay", no_delay_queue)
        self.no_delay = no_delay_queue.put

        staggered_queue = midi_out_staggered_queue(
                              print_, "global_int7-staggered", 0.01)
        staggered_queue.open()
        self.staggered = staggered_queue.put
