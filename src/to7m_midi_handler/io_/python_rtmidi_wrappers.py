from multiprocessing import Process, Queue
from rtmidi import MidiIn, MidiOut

from . import INTERNAL_CLIENT_NAME
from .staggered_queue import StaggeredQueue


dont_delete = []


def _queue_to_send(queue, send):
    map(send, iter(queue.get, object()))


def midi_in_set_queue(name, controller_queue, port_purpose):
    cq_put = controller_queue.put

    def callback(msg, _):
        cq_put((port_purpose, msg[0]))

    port = MidiIn(name=INTERNAL_CLIENT_NAME)
    port.open_virtual_port(name)
    port.ignore_types(False)
    port.set_callback(callback, data=True)

    dont_delete.append(port)


def midi_out_return_send(name):
    port = MidiOut(name=INTERNAL_CLIENT_NAME)
    port.open_virtual_port(name)
    return port.send_message


def midi_out_set_queue(name, queue):
    send = midi_out_return_send(name)
    Process(target=_queue_to_send, args=(queue, send)).start()


def midi_out_staggered_queue(print_, name, delay):
    queue = Queue()
    midi_out_set_queue(name, queue)
    staggered_queue = StaggeredQueue(print_, delay, queue)
    return staggered_queue
