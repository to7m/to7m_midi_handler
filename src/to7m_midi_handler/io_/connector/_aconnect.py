from subprocess import check_output, Popen, DEVNULL, PIPE

from ...exceptions import AconnectParseError, AconnectConnectError


"""
This module could be extracted and made into its own library. Nothing here
should be specific to to7m_midi_handler; that stuff goes in
./_internal_state.py.
"""


def _repr_str(instance, **kwargs):
    cls = type(instance)
    cls_str = '.'.join((cls.__module__, cls.__qualname__))
    kwarg_strs = (f"{key}={repr(val)}" for key, val in kwargs.items())
    return f"{cls_str}({', '.join(kwarg_strs)})"


class Connection:
    def __init__(self,
                 from_client_num, from_port_num, to_client_num, to_port_num):
        self.from_client_num = from_client_num
        self.from_port_num = from_port_num
        self.to_client_num = to_client_num
        self.to_port_num = to_port_num

    def __repr__(self):
        return _repr_str(self,
                         from_client_num=self.from_client_num,
                         from_port_num=self.from_port_num,
                         to_client_num=self.to_client_num,
                         to_port_num=self.to_port_num)
    """
    @classmethod
    def from_conn_line(cls, own_client_num, own_port_num, conn_line):
        segments = conn_line.split(':')
        print(segments)
        direction, other_client_num, other_port_num = segments
        other_client_num = int(other_client_num)
        other_port_num = int(other_port_num)

        if "From" in direction:
            return cls(other_client_num, other_port_num,
                       own_client_num, own_port_num)
        elif "To" in direction:
            return cls(own_client_num, own_port_num,
                       other_client_num, other_port_num)
        else:
            raise AconnectParseError(
                f"invalid direction in connection line {conn_line}"
            )
    """

    @classmethod
    def from_ports(cls, from_port, to_port):
        return cls(from_port.client_num, from_port.num,
                   to_port.client_num, to_port.num)

    def connect(self, disconnect=False):
        from_ = f"{self.from_client_num}:{self.from_port_num}"
        to = f"{self.to_client_num}:{self.to_port_num}"

        command = ["aconnect", from_, to]
        if disconnect:
            command.insert(1, '-d')

        proc = Popen(command, stdout=DEVNULL, stderr=PIPE)
        _, raw_err = proc.communicate()

        if raw_err:
            action = "disconnect" if disconnect else "connect"
            from_or_to = "from" if disconnect else "to"
            raise AconnectConnectError(
                      f"failed to {action} {from_} {from_or_to} {to}")

    def disconnect(self):
        self.connect(disconnect=True)


class _ConnectionsBase:
    def __init__(self, list_of_conns):
        self._list_of_conns = list_of_conns

    def __repr__(self):
        return _repr_str(self, list_of_conns=self._list_of_conns)

    def __iter__(self):
        return iter(self._list_of_conns)

    def __bool__(self):
        return bool(self._list_of_conns)

    def __len__(self):
        return len(self._list_of_conns)

    def __getitem__(self, index):
        return self._list_of_conns[index]

    def __contains__(self, item):
        return item in self._list_of_conns

    @staticmethod
    def _other_info_gen(conns_line):
        for info in ':'.join(conns_line.split(':')[1:]).split(', '):
            yield tuple(map(int, info.split(':')))

    def append(self, item):
        self._list_of_conns.append(item)

    def remove(self, item):
        self._list_of_conns.remove(item)

    def disconnect_all(self):
        for conn in self:
            conn.disconnect()

        self._list_of_conns = []


class Froms(_ConnectionsBase):
    @classmethod
    def from_froms_line(cls, own_client_num, own_port_num, froms_line):
        list_of_froms = [
            Connection(other_client_num, other_port_num,
                       own_client_num, own_port_num)
            for other_client_num, other_port_num in cls._other_info_gen(
                                                        froms_line)
        ]

        return cls(list_of_froms)


class Tos(_ConnectionsBase):
    @classmethod
    def from_tos_line(cls, own_client_num, own_port_num, tos_line):
        list_of_tos = [
            Connection(own_client_num, own_port_num,
                       other_client_num, other_port_num)
            for other_client_num, other_port_num in cls._other_info_gen(
                                                        tos_line)
        ]

        return cls(list_of_tos)


class Port:
    def __init__(self, client_num, port_num, port_name, froms, tos):
        self.client_num = client_num
        self.num = port_num
        self.name = port_name
        self.froms = froms
        self.tos = tos

    def __repr__(self):
        return _repr_str(self, client_num=self.client_num,
                         port_num=self.num, port_name=self.name,
                         froms=self.froms, tos=self.tos)

    @classmethod
    def from_port_lines(cls, client_num, port_lines):
        header_line, *connections_lines = port_lines

        a, port_name, _ = header_line.split("'")
        port_num = int(a)

        froms_line, tos_line = None, None
        for connections_line in connections_lines:
            if "From" in connections_line:
                froms_line = connections_line
            elif "To" in connections_line:
                tos_line = connections_line
            else:
                raise AconnectParseError(
                          f"can't parse connections line {connections_line}")

        if froms_line:
            froms = Froms.from_froms_line(client_num, port_num, froms_line)
        else:
            froms = Froms([])

        if tos_line:
            tos = Tos.from_tos_line(client_num, port_num, tos_line)
        else:
            tos = Tos([])

        return cls(client_num, port_num, port_name, froms, tos)

    def connect_to(self, other):
        conn = Connection.from_ports(self, other)
        conn.connect()
        other.froms.append(conn)
        self.tos.append(conn)

    def disconnect_all(self):
        self.froms.disconnect_all()
        self.tos.disconnect_all()


class Ports:
    def __init__(self, list_of_ports):
        self._list_of_ports = list_of_ports
        self._ports_dict = self._get_ports_dict()

    def __repr__(self):
        return _repr_str(self, list_of_ports=self._list_of_ports)

    def __iter__(self):
        return iter(self._list_of_ports)

    def __bool__(self):
        return bool(self._list_of_ports)

    def __len__(self):
        return len(self._list_of_ports)

    def __getitem__(self, index):
        if isinstance(index, str):
            index = index.ljust(16, ' ')

        return self._ports_dict[index]

    def __contains__(self, item):
        return item in self._ports_dict or item in self._list_of_ports

    def _get_ports_dict(self):
        ports_dict = {}
        for port in self:
            for index in (port.num, port.name):
                if index not in ports_dict:
                    ports_dict[index] = []

                ports_dict[index].append(port)
        return ports_dict

    @classmethod
    def from_ports_lines(cls, client_num, ports_lines):
        list_of_ports = []
        port_lines = [ports_lines[0]]
        for ports_line in ports_lines[1:]:
            if ports_line.startswith('\t'):
                port_lines.append(ports_line)
            else:
                list_of_ports.append(Port.from_port_lines(client_num,
                                                          port_lines))
                port_lines = [ports_line]
        list_of_ports.append(Port.from_port_lines(client_num, port_lines))

        return cls(list_of_ports)

    def remove(self, index):
        port = self[index][0]
        self._list_of_ports.remove(port)
        for key in (port.num, port.name):
            self._ports_dict[key].remove(port)
            if not self._ports_dict[key]:
                del self._ports_dict[key]


class Client:
    def __init__(self, client_num, client_name, ports):
        self.num = client_num
        self.name = client_name
        self.ports = ports

    def __repr__(self):
        return _repr_str(self, client_num=self.num, client_name=self.name,
                         ports=self.ports)

    @classmethod
    def from_client_lines(cls, client_lines):
        header_line, *ports_lines = client_lines

        a, client_name, _ = header_line.split("'")
        client_num = int(a[7:-2])

        ports = Ports.from_ports_lines(client_num, ports_lines)

        return cls(client_num, client_name, ports)


class Clients:
    def __init__(self, list_of_clients):
        self._list_of_clients = list_of_clients
        self._clients_dict = self._get_clients_dict()

    def __repr__(self):
        return _repr_str(self, list_of_clients=self._list_of_clients)

    def __iter__(self):
        return iter(self._list_of_clients)

    def __bool__(self):
        return bool(self._list_of_clients)

    def __len__(self):
        return len(self._list_of_clients)

    def __getitem__(self, index):
        return self._clients_dict[index]

    def __contains__(self, item):
        return item in self._clients_dict or item in self._list_of_clients

    def _get_clients_dict(self):
        clients_dict = {}
        for client in self:
            for index in (client.num, client.name):
                if index not in clients_dict:
                    clients_dict[index] = []

                clients_dict[index].append(client)
        return clients_dict

    @classmethod
    def from_clients_lines(cls, clients_lines):
        list_of_clients = []
        client_lines = [clients_lines[0]]
        for clients_line in clients_lines[1:]:
            if clients_line.startswith("client"):
                list_of_clients.append(Client.from_client_lines(client_lines))
                client_lines = [clients_line]
            else:
                client_lines.append(clients_line)
        list_of_clients.append(Client.from_client_lines(client_lines))

        return cls(list_of_clients)

    def remove(self, index):
        client = self[index][0]
        self._list_of_clients.remove(client)
        for key in (client.num, client.name):
            self._clients_dict[key].remove(client)
            if not self._clients_dict[key]:
                del self._clients_dict[key]


class Aconnect:
    def __init__(self, aconnect_raw_output=None):
        if aconnect_raw_output is None:
            self._aconnect_raw_output = self.raw_output()
        else:
            self._aconnect_raw_output = aconnect_raw_output

        clients_lines = self._aconnect_raw_output.decode().split('\n')[:-1]
        self.clients = Clients.from_clients_lines(clients_lines)

    def __repr__(self):
        return _repr_str(self, aconnect_raw_output=self._aconnect_raw_output)

    @staticmethod
    def raw_output():
        return check_output(["aconnect", '-l'])
