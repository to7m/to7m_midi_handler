from time import sleep
from multiprocessing import Process

from ...exceptions import AconnectConnectError
from ._udev import UdevEvents
from ._aconnect import Aconnect
from ._internal_state import IsChanged, InternalState


class Connector(Process):
    def __init__(self, print_, profiles_dict, vcs_injects, global_int7_inject):
        super().__init__()

        self._print = print_
        self._profiles_dict = profiles_dict
        self._vcs_injects = vcs_injects
        self._global_int7_inject = global_int7_inject

        self._saved_aconnect_raw_output = None
        self._saved_internal_state_dict = None

    def _compare_dicts(self, current_internal_state_dict):
        is_diff = IsChanged()

        for vc_num, val in enumerate(self._saved_internal_state_dict["vcs"]):
            if val != current_internal_state_dict["vcs"][vc_num]:
                is_diff.vcs[vc_num] = True

        if (self._saved_internal_state_dict["int7"]
                != current_internal_state_dict["int7"]):
            is_diff.int7 = True

        return is_diff

    def _vc_send_disconnect(self, vc_num):
        self._vcs_injects[vc_num]((("connection change", None),))

    def _vc_send_connect(self, vc_num, profile_name):
        self._vc_send_disconnect(vc_num)
        self._vcs_injects[vc_num]((("connection change", profile_name),))

    def _int7_send_disconnect(self):
        self._global_int7_inject(("disconnect", ()))

    def _int7_send_connect(self):
        self._int7_send_disconnect()
        self._global_int7_inject(("connect", ()))

    def _resolve_internal_state(self,
                                is_changed_any, current_internal_state_dict):
        if not is_changed_any:
            if self._saved_internal_state_dict:
                self._print("internal connections not changed")

            return

        for vc_num, is_changed in enumerate(is_changed_any.vcs):
            if is_changed:
                profile_name = current_internal_state_dict["vcs"][vc_num]
                if profile_name:
                    self._vc_send_connect(vc_num, profile_name)
                    self._print(f"profile {profile_name} connected "
                                f"and assigned to vc{vc_num}")
                elif (self._saved_internal_state_dict
                      and self._saved_internal_state_dict["vcs"][vc_num]):
                    self._vc_send_disconnect(vc_num)
                    self._print(f"profile for vc {vc_num} unassigned")

        if is_changed_any.int7:
            if current_internal_state_dict["int7"]:
                self._int7_send_connect()
                self._print("int7 connected")
            elif (self._saved_internal_state_dict
                  and self._saved_internal_state_dict["int7"]):
                self._int7_send_disconnect()
                self._print("int7 disconnected")

    def _update_connections(self):
        aconnect_raw_output = Aconnect.raw_output()
        if aconnect_raw_output == self._saved_aconnect_raw_output:
            return

        if self._saved_internal_state_dict:
            self._print("change found in ALSA MIDI state")

        is_changed_any = IsChanged()
        while True:
            try:
                internal_state = InternalState(self._profiles_dict,
                                               aconnect_raw_output)
                more_checks_needed = internal_state.resolve_connections()
                if not more_checks_needed:
                    break
            except AconnectConnectError as err:
                self._print(
                    "failed connection in Connector._update_connections():",
                    err
                )

            is_changed_any |= internal_state.is_changed
            aconnect_raw_output = Aconnect.raw_output()

        current_internal_state_dict = internal_state.dict()
        if self._saved_internal_state_dict:
            is_changed_any |= self._compare_dicts(current_internal_state_dict)

        self._resolve_internal_state(is_changed_any,
                                     current_internal_state_dict)

        self._saved_aconnect_raw_output = aconnect_raw_output
        self._saved_internal_state_dict = current_internal_state_dict

    def run(self):
        while True:
            try:
                with UdevEvents() as udev_events:
                    while True:
                        self._update_connections()
                        sleep(0.3)
                        udev_events.block_until_events()
            except Exception as err:
                self._print("error in connector:", err)
