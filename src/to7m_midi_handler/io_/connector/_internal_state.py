from ...exceptions import NoAvailableClientError


from .. import INTERNAL_CLIENT_NAME
from . import INT7_CLIENT_NAME
from ._aconnect import Ports, Clients, Aconnect


class IsChanged:
    def __init__(self):
        self.vcs = [False] * 4
        self.int7 = False

    def __bool__(self):
        bool_ = False
        for val in self.vcs:
            bool_ |= val
        bool_ |= self.int7
        return bool_

    def __ior__(self, other):
        for i, val in enumerate(other.vcs):
            self.vcs[i] |= val

        self.int7 |= other.int7

        return self


class InternalState:
    """
    None: not connected
    False: connected but corrupted (ALSA MIDI connections present)
    True: fully connected (if int7)
    str: fully connected where the str is a profile name
    """

    def __init__(self, profiles_dict, aconnect_raw_output):
        self._profiles_dict = profiles_dict
        self._aconnect = Aconnect(aconnect_raw_output)
        self._internal_ports = self._get_internal_ports()
        self._current = self._get_current()
        self._target = None

        self.is_changed = IsChanged()

    def _get_internal_ports(self):
        list_of_internal_ports = []
        for client in self._aconnect.clients:
            if client.name == INTERNAL_CLIENT_NAME:
                for port in client.ports:
                    list_of_internal_ports.append(port)
        return Ports(list_of_internal_ports)

    def _max_connections(self, in_ports, out_ports):
        max_connections = 0

        for port in in_ports.values():
            max_connections = max(max_connections, len(port.froms))

        for port in out_ports.values():
            max_connections = max(max_connections, len(port.tos))

        return max_connections

    def _matches_profile(self, list_of_unclaimed_clients,
                         in_ports, out_ports, profile):
        client_nums_for_indices, client_indices_for_nums = {}, {}
        for i in range(2):
            connect_info_dict = (profile.connect_info.in_,
                                 profile.connect_info.out)[i]
            internal_ports = (in_ports, out_ports)[i]
            for port_purpose, port_info in connect_info_dict.items():
                conns = (internal_ports[port_purpose].froms,
                         internal_ports[port_purpose].tos)[i]

                if port_info and conns:
                    conn = conns[0]
                elif port_info or conns:
                    return False
                else:
                    continue

                client_i, port_num = port_info
                client_num = (conn.from_client_num, conn.to_client_num)[i]

                i_noted = client_i in client_nums_for_indices
                num_noted = client_num in client_indices_for_nums
                if i_noted and num_noted:
                    client_name_a = profile.connect_info.client_names[client_i]
                    client_name_b = self._aconnect.clients[client_num][0].name
                    if client_name_a != client_name_b:
                        return False
                elif i_noted or num_noted:
                    return False
                else:
                    client_nums_for_indices[client_i] = client_num
                    client_indices_for_nums[client_num] = client_i

                if port_num != (conn.from_port_num, conn.to_port_num)[i]:
                    return False

        new_unclaimed_clients = Clients(list_of_unclaimed_clients)
        for client_num in client_indices_for_nums:
            if client_num not in new_unclaimed_clients:
                return False

            new_unclaimed_clients.remove(client_num)
        return new_unclaimed_clients

    def _vc_current(self, list_of_unclaimed_clients, controller_num):
        in_ports, out_ports = {}, {}
        for i in range(4):
            for direction, dict_ in (("in", in_ports), ("out", out_ports)):
                port_purpose = f"{direction}{i}"
                key = f"controller{controller_num}-{port_purpose}"
                dict_[port_purpose] = self._internal_ports[key][0]

        max_connections = self._max_connections(in_ports, out_ports)
        if max_connections == 0:
            return list_of_unclaimed_clients, None
        elif max_connections > 1:
            return list_of_unclaimed_clients, False

        for profile_name, profile in self._profiles_dict.items():
            new_unclaimed_clients = self._matches_profile(
                list_of_unclaimed_clients, in_ports, out_ports, profile
            )
            if new_unclaimed_clients:
                return new_unclaimed_clients, profile_name

        return list_of_unclaimed_clients, False

    def _vcs_current(self):
        list_of_unclaimed_clients = list(self._aconnect.clients)
        vcs_current = []
        for controller_num in range(4):
            new_unclaimed_clients, vc_current = self._vc_current(
                list_of_unclaimed_clients, controller_num
            )
            list_of_unclaimed_clients = list(new_unclaimed_clients)
            vcs_current.append(vc_current)

        return vcs_current

    def _num_of_internal_conns(self, port):
        num_of_internal_conns = 0

        for conns in port.froms, port.tos:
            for conn in conns:
                for client_num in conn.from_client_num, conn.to_client_num:
                    client = self._aconnect.clients[client_num][0]
                    if client.name == INTERNAL_CLIENT_NAME:
                        num_of_internal_conns += 1

        return num_of_internal_conns

    def _int7_current(self):
        if INT7_CLIENT_NAME not in self._aconnect.clients:
            return None

        client = self._aconnect.clients[INT7_CLIENT_NAME][0]

        num_of_internal_conns = self._num_of_internal_conns(client.ports[0][0])
        if num_of_internal_conns == 0:
            return None
        elif num_of_internal_conns != 6:
            return False
        else:
            return True

    def _get_current(self):
        vcs_current = self._vcs_current()
        int7_current = self._int7_current()
        return {"vcs": vcs_current, "int7": int7_current}

    def _unordered_profile_names(self):
        list_of_unclaimed_clients = list(self._aconnect.clients)
        viable_profile_names = []
        for profile_name, profile in self._profiles_dict.items():
            profile_viable = True
            while profile_viable:
                new_unclaimed_clients = Clients(list_of_unclaimed_clients)

                for client_name in profile.connect_info.client_names:
                    if client_name in new_unclaimed_clients:
                        new_unclaimed_clients.remove(client_name)
                    else:
                        profile_viable = False
                        break

                if profile_viable:
                    list_of_unclaimed_clients = list(new_unclaimed_clients)
                    viable_profile_names.append(profile_name)

        return viable_profile_names[:4]

    def _vcs_target(self):
        unordered_profile_names = self._unordered_profile_names()

        vcs_target = [None] * 4
        unclaimed_vc_nums = set(range(4))
        remaining_unordered_profile_names = []
        for profile_name in unordered_profile_names:
            added = False
            for vc_num in unclaimed_vc_nums:
                if self._current["vcs"][vc_num] == profile_name:
                    vcs_target[vc_num] = profile_name
                    unclaimed_vc_nums.remove(vc_num)
                    added = True
                    break

            if not added:
                remaining_unordered_profile_names.append(profile_name)
        for profile_name in remaining_unordered_profile_names:
            vc_num = unclaimed_vc_nums.pop()
            vcs_target[vc_num] = profile_name

        return vcs_target

    def _int7_target(self):
        return True if INT7_CLIENT_NAME in self._aconnect.clients else None

    def _set_target(self):
        vcs_target = self._vcs_target()
        int7_target = self._int7_target()
        self._target = {"vcs": vcs_target, "int7": int7_target}

    def _vc_disconnect(self, vc_num):
        for port_purpose_direction in "in", "out":
            for port_purpose_num in range(4):
                port_purpose = f"{port_purpose_direction}{port_purpose_num}"
                port_name = f"controller{vc_num}-{port_purpose}"
                port = self._internal_ports[port_name][0]
                port.disconnect_all()

    def _vc_client_nums(self, vc_num, profile):
        client_nums = []
        for client_name in profile.connect_info.client_names:
            client_found = False
            for client in self._aconnect.clients[client_name]:
                conn_found = False
                for port in client.ports:
                    conn_found |= bool(self._num_of_internal_conns(port))

                if not conn_found:
                    client_found = True
                    client_nums.append(client.num)
                    break

            if not client_found:
                raise NoAvailableClientError(
                    f"clients with name {repr(client_name)} all occupied"
                )

        return client_nums

    def _vc_connect(self, vc_num, profile):
        client_nums = self._vc_client_nums(vc_num, profile)

        for port_purpose_name, dict_ in (("in", profile.connect_info.in_),
                                         ("out", profile.connect_info.out)):
            for port_purpose, port_info in dict_.items():
                if port_info is None:
                    continue

                client_i, other_port_num = port_info
                other_client_num = client_nums[client_i]
                other_client = self._aconnect.clients[other_client_num][0]
                other_port = other_client.ports[other_port_num][0]

                own_port_name = f"controller{vc_num}-{port_purpose}"
                own_port = self._internal_ports[own_port_name][0]

                if port_purpose_name == "in":
                    other_port.connect_to(own_port)
                elif port_purpose_name == "out":
                    own_port.connect_to(other_port)

    def _vcs_resolve_connections(self):
        vcs_action_taken = [
            self._current["vcs"][vc_num] != self._target["vcs"][vc_num]
            for vc_num in range(4)
        ]

        for vc_num, action_taken in enumerate(vcs_action_taken):
            if action_taken:
                self.is_changed.vcs[vc_num] = True
                self._vc_disconnect(vc_num)

        for vc_num, action_taken in enumerate(vcs_action_taken):
            profile_name = self._target["vcs"][vc_num]
            if action_taken and profile_name:
                profile = self._profiles_dict[profile_name]
                self._vc_connect(vc_num, profile)

        return any(vcs_action_taken)

    def _int7_disconnect(self):
        for port in self._internal_ports:
            if "int7" in port.name:
                port.disconnect_all()

    def _int7_connect(self):
        port_names = [f"controller{vc_num}-int7" for vc_num in range(4)]
        port_names.append("global_int7-no_delay")
        port_names.append("global_int7-staggered")

        int7_port = self._aconnect.clients[INT7_CLIENT_NAME][0].ports[0][0]
        for port_name in port_names:
            port = self._internal_ports[port_name][0]
            port.connect_to(int7_port)

    def _int7_resolve_connections(self):
        int7_action_taken = self._current["int7"] != self._target["int7"]

        if int7_action_taken:
            self.is_changed.int7 = True
            self._int7_disconnect()
            if self._target["int7"]:
                self._int7_connect()

        return int7_action_taken

    def resolve_connections(self):
        self._set_target()

        actions_taken = False
        actions_taken |= self._vcs_resolve_connections()
        actions_taken |= self._int7_resolve_connections()

        return actions_taken

    def dict(self):
        return self._current
