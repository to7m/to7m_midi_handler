from time import sleep
from queue import Queue, Empty
import pyudev


class UdevEvents:
    def __init__(self):
        self.queue = Queue()

        monitor = pyudev.Monitor.from_netlink(pyudev.Context())
        monitor.filter_by("usb", device_type="usb_device")
        self.observer = pyudev.MonitorObserver(monitor,
                                               callback=self._add_to_queue)

    def __enter__(self):
        self.observer.start()
        return self

    def __exit__(self, err_type, err_value, traceback):
        self.observer.stop()

    def _add_to_queue(self, device):
        self.queue.put("action detected")

    def block_until_events(self, first_event_delay=0.06, recheck_delay=0.01):
        self.queue.get()
        sleep(first_event_delay)

        while True:
            try:
                while True:
                    self.queue.get(False)
            except Empty:
                sleep(recheck_delay)
                try:
                    self.queue.get(False)
                except Empty:
                    return
