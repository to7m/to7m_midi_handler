from time import sleep
from ctypes import c_bool
from threading import Thread
from queue import Queue, Empty
import multiprocessing as mp

from ..exceptions import StaggeredQueueError


STOP = None


class _SharedState:
    def __init__(self):
        manager = mp.Manager()

        self.buffer_lock = mp.Lock()
        self.items_in_channels = manager.dict()
        self.channel_keys = manager.list()

        self.is_open = mp.Value(c_bool, False)


class StaggeredQueue:
    def __init__(self, print_, delay, target_queue=None):
        super().__init__()

        self._print = print_
        self._delay = delay

        if target_queue is None:
            self._target_queue = mp.Queue()
        else:
            self._target_queue = target_queue
        self._internal_queue = mp.Queue()
        self._notification_queue = Queue()

        self._shared_state = _SharedState()

    def __enter__(self):
        return self.open()

    def __exit__(self, err_type, err_val, traceback):
        self.close()

    def _transfer(self):
        for notification in iter(self._notification_queue.get, object()):
            try:
                while True:
                    if notification is STOP:
                        return
                    notification = self._notification_queue.get(False)
            except Empty:
                pass

            while True:
                with self._shared_state.buffer_lock:
                    if self._shared_state.channel_keys:
                        key = self._shared_state.channel_keys.pop(0)
                        item = self._shared_state.items_in_channels.pop(key)
                        self._target_queue.put(item)
                    else:
                        break

                sleep(self._delay)

    def _run(self):
        transfer_thread = Thread(target=self._transfer)
        transfer_thread.start()

        for obj in iter(self._internal_queue.get, object()):
            try:
                key, item = obj
                with self._shared_state.buffer_lock:
                    if key not in self._shared_state.items_in_channels:
                        self._shared_state.channel_keys.append(key)

                    self._shared_state.items_in_channels[key] = item
                self._notification_queue.put(True)
            except Exception as err:
                if obj is STOP:
                    self._notification_queue.put(STOP)
                    transfer_thread.join()
                    return
                else:
                    self._print("staggered sending caused error:", err)

    def open(self):
        mp.Process(target=self._run).start()
        self._shared_state.is_open.value = True

        return self

    def close(self):
        self._shared_state.is_open.value = False
        self._internal_queue.put(STOP)

    def put(self, channel_name, item):
        if not self._shared_state.is_open:
            raise StaggeredQueueError("StaggeredQueue instance not opened")

        self._internal_queue.put((channel_name, item))

    def get(self):
        if not self._shared_state.is_open:
            raise StaggeredQueueError("StaggeredQueue instance not opened")

        return self._target_queue.get()
