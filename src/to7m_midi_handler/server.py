from time import sleep

from .patches import get_patches
from .controller import get_profiles, VirtualController
from .int7 import GlobalInt7
from .io_ import Connector


class Server:
    def __init__(self, print_):
        patches_dict = get_patches()
        profiles_dict = get_profiles(print_, patches_dict)

        self._vcs = [
            VirtualController(print_, vc_num, profiles_dict, patches_dict)
            for vc_num in range(4)
        ]
        vcs_injects = [vc.inject for vc in self._vcs]

        self._global_int7 = GlobalInt7(print_, vcs_injects)
        global_int7_inject = self._global_int7.inject

        self._connector = Connector(print_, profiles_dict,
                                    vcs_injects, global_int7_inject)

    def run(self):
        for vc in self._vcs:
            vc.start()
        self._global_int7.start()
        self._connector.start()

        while True:
            sleep(1)
