from multiprocessing import Process, Queue

from ..io_.ports import GlobalInt7Ports


class _State:
    def __init__(self, vcs_injects):
        self._vcs_injects = vcs_injects

    def connect(self):
        for vc_inject in self._vcs_injects:
            vc_inject((("connect to int7", None),))

    def disconnect(self):
        pass


class GlobalInt7(Process):
    def __init__(self, print_, vcs_injects):
        super().__init__()

        queue = Queue()
        ports = GlobalInt7Ports(print_)

        self._state = _State(vcs_injects)

        self._receive = queue.get
        self.inject = queue.put

    def run(self):
        state = self._state
        for state_attr, xargs in iter(self._receive, object()):
            getattr(state, state_attr)(*xargs)
