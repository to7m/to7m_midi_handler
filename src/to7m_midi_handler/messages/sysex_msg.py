class SysexMsg:
    def __init__(self, before=[], address=[], after=[]):
        self.before = before
        self.address = address
        self.after = after

    def _address_from_offset(self, offset):
        return [(address_byte + offset_byte) % 128
                for address_byte, offset_byte in zip(self.address, offset)]

    def replace_elements(self, before=None, address=None, after=None):
        return SysexMsg((self.before if before is None else before),
                        (self.address if address is None else address),
                        (self.after if after is None else after))

    def offset(self, offset):
        address = self._address_from_offset(offset)
        return SysexMsg(self.before_address, address, self.after_address)

    def byte_list(self, offset=None, between_address_and_after=[]):
        if offset is None:
            address = self.address
        else:
            address = self._address_from_offset(offset)

        return [240, *self.before,
                *address, *between_address_and_after,
                *self.after, 247]
