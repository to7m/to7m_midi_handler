class To7mMidiHandlerError(Exception):
    pass


class MsgHandlingError(To7mMidiHandlerError):
    pass


class ConfigToolsError(To7mMidiHandlerError):
    pass


class AconnectError(To7mMidiHandlerError):
    pass


class AconnectParseError(AconnectError):
    pass


class AconnectConnectError(AconnectError):
    pass


class NoAvailableClientError(To7mMidiHandlerError):
    pass


class StaggeredQueueError(To7mMidiHandlerError):
    pass


class PatchConfigError(To7mMidiHandlerError):
    pass


"""

class ControllerError(To7mMidiHandlerError):
    pass


class ControllerRealError(ControllerError):
    pass



class ReproductionError(ConfigToolsError):
    pass


class InvalidCallbackFactoryError(ConfigToolsError):
    pass


class ControllerVirtualError(ControllerError):
    pass


class MsgHandlerError(ControllerVirtualError):
    pass


class InvalidControllerModeError(MsgHandlerError):
    def __init__(self, mode):
        self.mode = mode
        super().__init__(f"invalid controller mode: {mode}")


class InvalidSignError(MsgHandlerError):
    def __init__(self, sign):
        self.sign = sign
        super().__init__(f"invalid sign (should be 1 or -1): {sign}")


class InvalidDigitError(MsgHandlerError):
    def __init__(self, digit):
        self.digit = digit
        super().__init__(f"invalid digit (should be int from 0 to 9): {digit}")


class ChangeNeeded(ControllerVirtualError):
    def __init__(self, profile_name):
        self.profile_name = profile_name
        super().__init__()
"""
